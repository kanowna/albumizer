#include "IndexWriter.h"
#include "ThumbnailWriter.h"
#include <boost/algorithm/string.hpp>
#include <boost/regex.hpp>
#include <fstream>
//#include <iostream>
#include <sstream>

namespace fs = boost::filesystem;

const size_t IndexWriter::DEPTH_MAX = 3;
const uint32_t IndexWriter::GOBBLE_SIZE = 0x400;
const std::string IndexWriter::FAVICON("favicon.png");
const std::string IndexWriter::ICON_UP("upIcon.png");
const std::string IndexWriter::ICON_LEFT("leftIcon.png");
const std::string IndexWriter::ICON_RIGHT("rightIcon.png");
const std::string IndexWriter::IMAGES_DIR("images");
const std::string IndexWriter::INDEX_HTML("index.html");
const std::string IndexWriter::SEPARATOR(1, fs::path::preferred_separator);

IndexWriter::IndexWriter(const ConfigMap& config, const fs::path& indexPath, const fs::path& sitePath, const fs::path& stylePath)
: m_config(config),
  m_indexPath(indexPath),
  m_sitePath(sitePath),
  m_stylePath(stylePath)
{
}

fs::path IndexWriter::getSitePath(const fs::path& rootPath)
{
    fs::path sitePath;
    fs::path ancestorPath(rootPath);

    for (auto depth=0; depth<DEPTH_MAX; depth++)
    {
        ancestorPath = ancestorPath.parent_path();
        fs::path indexPath(ancestorPath.string() + SEPARATOR + INDEX_HTML);

        if (!fs::is_directory(ancestorPath))
        {
            break;
        }

        else if (fs::exists(indexPath))
        {
            sitePath = indexPath;
            break;
        }
    }

    return sitePath;
}

void IndexWriter::insertAlbumName(const std::string& albumName)
{
    m_albumSet.insert(albumName);
}

void IndexWriter::insertImage(const fs::path& imagePath, const size_t width, const size_t height)
{
    m_imageMap.insert(ImgPairType(imagePath, XYPairType(width, height)));
}

bool IndexWriter::write()
{
    bool status = EXIT_FAILURE;
    std::string inputBuffer;
    std::ifstream inputStream(m_indexPath.string(), std::ios_base::in | std::ios::binary);

    while (inputStream.good())
    {
        char gobble[GOBBLE_SIZE] = {0};
        inputStream.read(&gobble[0], GOBBLE_SIZE);
        inputBuffer.append(gobble, inputStream.gcount());
    }
    inputStream.close();

    std::ostringstream outputStream(m_indexPath.string(), std::ios_base::out | std::ios::trunc);
    if (outputStream.good())
    {
        fs::path albumPath(m_indexPath.parent_path().string());
        fs::path imagesRelativePath(fs::relative(m_sitePath.parent_path().string() + SEPARATOR + IMAGES_DIR, albumPath));
        fs::path siteRelativePath(fs::relative(m_sitePath, albumPath));
        fs::path styleRelativePath(fs::relative(m_stylePath, albumPath));
        std::string albumName(albumPath.filename().string());
        std::string albumTitle(getAlbumName(albumName));
        std::string analyticsId(m_config.getConfig(ConfigMap::ANALYTICS_ID));
        bool chronological = (m_config.getConfig(ConfigMap::CHRONOLOGICAL) == "1");

        outputStream << "<!DOCTYPE html>" << std::endl;
        outputStream << "<!-- Created by albumizer (https://bitbucket.org/kanowna/albumizer/src/master/) -->" << std::endl;
        outputStream << std::endl;
        outputStream << "<html>" << std::endl;
        outputStream << "  <head>" << std::endl;
        outputStream << "    <meta http-equiv='Content-Type' content='text/html; charset=UTF-8'>" << std::endl;
        outputStream << "    <meta name='viewport' content='width=device-width, initial-scale=1'>" << std::endl;
        outputStream << "    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css'>" << std::endl;
        outputStream << "    <link rel='stylesheet' href=" << styleRelativePath << " type='text/css'>" << std::endl;
        outputStream << "    <title>" << albumTitle << "</title>" << std::endl;
        outputStream << std::endl;

        if (!analyticsId.empty())
        {
            outputStream << "    <!-- Global site tag (gtag.js) - Google Analytics -->" << std::endl;
            outputStream << "    <script async src='https://www.googletagmanager.com/gtag/js?id=" << analyticsId << "'></script>" << std::endl;
            outputStream << "    <script>" << std::endl;
            outputStream << "      window.dataLayer = window.dataLayer || [];" << std::endl;
            outputStream << "      function gtag(){dataLayer.push(arguments);}" << std::endl;
            outputStream << "      gtag('js', new Date());" << std::endl;
            outputStream << "      gtag('config', '" << analyticsId << "');" << std::endl;
            outputStream << "    </script>" << std::endl;
            outputStream << std::endl;
        }

        outputStream << "    <script>" << std::endl;
        outputStream << "      var slideId = 0;" << std::endl;
        outputStream << std::endl;
        outputStream << "      function plusSlide(n) {" << std::endl;
        outputStream << "        showSlide(slideId + n);" << std::endl;
        outputStream << "      }" << std::endl;
        outputStream << std::endl;
        outputStream << "      function showSlide(id) {" << std::endl;
        outputStream << "        var currId = id;" << std::endl;
        outputStream << "        var slides = document.getElementsByClassName('slide');" << std::endl;
        outputStream << "        var thumbs = document.getElementsByClassName('thumb');" << std::endl;
        outputStream << std::endl;
        outputStream << "        if (currId >= slides.length) {" << std::endl;
        outputStream << "          currId = 0;" << std::endl;
        outputStream << "        }" << std::endl;
        outputStream << std::endl;
        outputStream << "        if (currId < 0) {" << std::endl;
        outputStream << "          currId = slides.length-1;" << std::endl;
        outputStream << "        }" << std::endl;
        outputStream << std::endl;
        outputStream << std::endl;
        outputStream << "        if (slides.length === thumbs.length) {" << std::endl;
        outputStream << "          for (ndx=0; ndx<slides.length; ndx++) {" << std::endl;
        outputStream << "            if (currId == slides[ndx].id) {" << std::endl;
        outputStream << "              slides[ndx].style.display='block';" << std::endl;
        outputStream << "              thumbs[ndx].style.display='none';" << std::endl;
        outputStream << "              slideId = currId;" << std::endl;
        outputStream << "            }" << std::endl;
        outputStream << "            else {" << std::endl;
        outputStream << "              slides[ndx].style.display='none';" << std::endl;
        outputStream << "              thumbs[ndx].style.display='block';" << std::endl;
        outputStream << "            }" << std::endl;
        outputStream << "          }" << std::endl;
        outputStream << "        }" << std::endl;
        outputStream << "      }" << std::endl;
        outputStream << "    </script>" << std::endl;
        outputStream << "  </head>" << std::endl;
        outputStream << std::endl;
        outputStream << "  <body>" << std::endl;
        outputStream << "    <nav class='navbar is-fixed-top is-link' role='navigation' aria-label='navigation'>" << std::endl;
        outputStream << "      <div class='container'>" << std::endl;
        outputStream << "        <div class='navbar-brand'>" << std::endl;
        outputStream << "          <a class='navbar-item' href=" << siteRelativePath << ">" << std::endl;
        outputStream << "            <img src='" << imagesRelativePath.string() << SEPARATOR << FAVICON << "' width='42' height='42' alt=''>" << std::endl;
        outputStream << "          </a>" << std::endl;
        outputStream << std::endl;
        outputStream << "          <span class='navbar-burger burger' data-target='navbarMenu' aria-label='menu' aria-expanded='false'>" << std::endl;
        outputStream << "            <span aria-hidden='true'></span>" << std::endl;
        outputStream << "            <span aria-hidden='true'></span>" << std::endl;
        outputStream << "            <span aria-hidden='true'></span>" << std::endl;
        outputStream << "          </span>" << std::endl;
        outputStream << "        </div>" << std::endl;
        outputStream << std::endl;
        outputStream << "        <div id='navbarMenu' class='navbar-menu'>" << std::endl;
        outputStream << "          <div class='navbar-end'>" << std::endl;
        outputStream << "            <a href='../" << INDEX_HTML << "' class='navbar-item'>" << std::endl;
        outputStream << "              <img src='" << imagesRelativePath.string() << SEPARATOR << ICON_UP << "' width='42' height='42' alt='Go back'>" << std::endl;
        outputStream << "            </a>" << std::endl;

        if (chronological)
        {
            for (auto albumIter = m_albumSet.begin(); albumIter!=m_albumSet.end(); albumIter++)
            {
                outputStream << "            <a href='" << *albumIter << "/index.html' class='navbar-item'>" << getAlbumName(*albumIter) << "</a>" << std::endl;
            }
        }
        else
        {
            for (auto albumIter = m_albumSet.rbegin(); albumIter!=m_albumSet.rend(); albumIter++)
            {
                outputStream << "            <a href='" << *albumIter << "/index.html' class='navbar-item'>" << getAlbumName(*albumIter) << "</a>" << std::endl;
            }
        }

        if (m_imageMap.size() > 1)
        {
            outputStream << "            <a href='javascript:plusSlide(-1);' class='navbar-item'>" << std::endl;
            outputStream << "              <img src='" << imagesRelativePath.string() << SEPARATOR << ICON_LEFT << "' width='42' height='42' alt='Prev slide'>" << std::endl;
            outputStream << "            </a>" << std::endl;

            outputStream << "            <a href='javascript:plusSlide(+1);' class='navbar-item'>" << std::endl;
            outputStream << "              <img src='" << imagesRelativePath.string() << SEPARATOR << ICON_RIGHT << "' width='42' height='42' alt='Next slide'>" << std::endl;
            outputStream << "            </a>" << std::endl;
        }

        outputStream << "          </div>" << std::endl;
        outputStream << "        </div>" << std::endl;
        outputStream << "      </div>" << std::endl;
        outputStream << "    </nav>" << std::endl;
        outputStream << std::endl;
        outputStream << "    <script type='text/javascript'>" << std::endl;
        outputStream << "      (function() {" << std::endl;
        outputStream << "        var burger = document.querySelector('.burger');" << std::endl;
        outputStream << "        var nav = document.querySelector('#'+burger.dataset.target);" << std::endl;
        outputStream << "        burger.addEventListener('click', function() {" << std::endl;
        outputStream << "          burger.classList.toggle('is-active');" << std::endl;
        outputStream << "          nav.classList.toggle('is-active');" << std::endl;
        outputStream << "        });" << std::endl;
        outputStream << "      })();" << std::endl;
        outputStream << "    </script>" << std::endl;
        outputStream << std::endl;
        outputStream << "    <hr>" << std::endl;
        outputStream << "    <div class='pudding'>" << std::endl;
        outputStream << "      <h1>" << albumTitle << "</h1>" << std::endl;
        outputStream << "      <ul>" << std::endl;

        if (chronological)
        {
            for (auto albumIter = m_albumSet.begin(); albumIter!=m_albumSet.end(); albumIter++)
            {
                outputStream << "        <li><a href='" << *albumIter << "/index.html'>" << getAlbumName(*albumIter) << "</a></li>" << std::endl;
            }
        }
        else
        {
            for (auto albumIter = m_albumSet.rbegin(); albumIter!=m_albumSet.rend(); albumIter++)
            {
                outputStream << "        <li><a href='" << *albumIter << "/index.html'>" << getAlbumName(*albumIter) << "</a></li>" << std::endl;
            }
        }

        outputStream << "      </ul>" << std::endl;
        outputStream << "    </div>" << std::endl;
        outputStream << std::endl;

        if (m_imageMap.size() > 1)
        {
            auto slideId = 0;
            outputStream << "    <ul class='flex-container'>" << std::endl;

            for (auto imageIter : m_imageMap)
            {
                fs::path imagePath(imageIter.first);
                std::string imageStem(imagePath.stem().string());
                std::string imageRelative(fs::relative(imagePath, albumPath).string());

                ThumbnailWriter thumb(imagePath);
                if (!thumb.exists())
                {
                    thumb.write();
                }

                std::string thumbRelative(fs::relative(thumb.getThumbPath(), albumPath).string());

                outputStream << "      <li>" << std::endl;
                outputStream << "        <img class='thumb' id=" << slideId << " src='" << thumbRelative << "' onclick='showSlide(" << slideId << ")'>" << std::endl;
                outputStream << "      </li>" << std::endl;

                outputStream << "      <li>" << std::endl;
                outputStream << "        <img class='slide' id=" << slideId << " src='" << imageRelative << "'>" << std::endl;
                outputStream << "      </li>" << std::endl;
                ++slideId;
            }

            outputStream << "    </ul>" << std::endl;
        }

        else if (m_imageMap.size() == 1)
        {
            std::string imageRelative(fs::relative(m_imageMap.begin()->first, albumPath).string());
            outputStream << "    <img src='" << imageRelative << "'>" << std::endl;
        }

        outputStream << "  </body>" << std::endl;
        outputStream << "</html>" << std::endl;
        status = EXIT_SUCCESS;
    }
  
    std::string outputBuffer(outputStream.str());
    if (inputBuffer != outputBuffer)
    {
        std::ofstream indexStream(m_indexPath.string(), std::ios_base::out | std::ios::trunc);
        indexStream << outputBuffer;
        indexStream.close();
    }

    return status;
}

std::string IndexWriter::getAlbumName(const std::string rawName)
{
    std::string albumName(rawName);
    boost::regex expression{"^(\\d{4}\\-\\d{2}\\-\\d{2}_)?(.*)$"};
    boost::smatch match;

    if ((boost::regex_search(rawName, match, expression)) &&
        (match.size() > 2))
    {
        albumName = match[2];
    }

    boost::replace_all(albumName, "_", " ");

    if (albumName == "albums")
    {
        albumName = "Albums";
    }
    return albumName;
}
