#include "ConfigMap.h"
#include <boost/container/flat_set.hpp>
#include <boost/filesystem.hpp>
#include <map>

namespace bc = boost::container;
namespace fs = boost::filesystem;

class IndexWriter
{
public:
    IndexWriter(const ConfigMap& config, const fs::path& indexPath, const fs::path& sitePath, const fs::path& stylePath);
    static fs::path getSitePath(const fs::path& rootPath);
    void insertAlbumName(const std::string& albumName);
    void insertImage(const fs::path& imagePath, const size_t width, const size_t height);
    bool write();

    static const size_t DEPTH_MAX;
    static const uint32_t GOBBLE_SIZE;
    static const std::string FAVICON;
    static const std::string ICON_LEFT;
    static const std::string ICON_UP;
    static const std::string ICON_RIGHT;
    static const std::string IMAGES_DIR;
    static const std::string INDEX_HTML;
    static const std::string SEPARATOR;

private:
    typedef bc::flat_set<std::string> AlbumSetType;
    typedef std::pair<size_t, size_t> XYPairType;
    typedef std::pair<fs::path, XYPairType> ImgPairType;
    typedef std::map<fs::path, XYPairType> ImageMapType;

    std::string getAlbumName(const std::string rawName);

    AlbumSetType m_albumSet;
    ImageMapType m_imageMap;

    const ConfigMap m_config;
    const fs::path m_indexPath;
    const fs::path m_sitePath;
    const fs::path m_stylePath;
};
