#include "ConfigMap.h"
#include <sstream>

ConfigMap::ConfigMap()
{
}

std::string ConfigMap::getConfig(const ConfigKeyType& key) const
{
    std::string value;
    auto configIter = m_configMap.find(key);
    if (configIter != m_configMap.end())
    {
        value = configIter->second;
    }

    return value;
}

void ConfigMap::setConfig(const ConfigKeyType& key, const bool value)
{
    std::ostringstream boolStream;
    boolStream << value;
    setConfig(key, boolStream.str());
}

void ConfigMap::setConfig(const ConfigMap::ConfigKeyType& key, const std::string value)
{
    m_configMap.insert(ConfigPairType(key, value));
}
