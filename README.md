albumizer V0.0.1
================

Intro
-----
Here's some info about building & running **albumizer** utility.

* An open-source WEB tool, for managing HTML5 Photo Album layouts.
* Albumizer recursively scans a directory-structure, containing JPEG & PNG image files.
* Albumizer auto-generates HTML5 Album page-layouts, that mirror the directory structure.
* Albumizer currently accepts [JPEG](https://en.wikipedia.org/wiki/JPEG) and [PNG](https://en.wikipedia.org/wiki/Portable_Network_Graphics) image formats.

Example
-------
Here's a sample album layout, that was generated using **albumizer** utility:

* http://andrewmacdonaldconsulting.com/albums/index.html

Building
--------
The following instructions should work OK on most Linux (and \*nix) platforms.
But it shouldn't be too difficult to build it on Mac or Win32, etc also.

Prerequisites
-------------
* C++ compiler
* cmake (version 3.12 or better)
* Boost .....
* ImageMagick++ library

From Package (recommended)
--------------
sudo apt install libmagick++-dev
sudo apt install libmagickwand-dev

From Source
-----------
#~sudo apt-get install libmagick++-dev
sudo apt install graphicsmagick-libmagick-dev-compat
Magick++ from source

#~./configure \
#~  --disable-sharing \
#~  --enable-shared=no \
#~  --prefix=$HOME/imageMagick \
#~  --with-modules \
#~  --with-jpeg \
#~  --with-jxl

./configure \
  --prefix=$HOME/.local \
  --with-modules \
  --with-jpeg \
  --with-jxl

make -j4
sudo make install

Compiling
---------
* cd _working directory_
* git clone https://bitbucket.org/kanowna/albumizer.git
* cd albumizer
* mkdir build
* cd build
* cmake ..
* make

Running
-------

> ./albumizer --help

> ./albumizer --album-path {Path to imagery folder}

> google-chrome {Path to imagery folder}
