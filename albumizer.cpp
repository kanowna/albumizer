#include "version.h"
#include "IndexWriter.h"
#include "StyleWriter.h"
#include "ThumbnailWriter.h"
#include <boost/filesystem.hpp>
#include <boost/program_options.hpp>
#include <fstream>
#include <iostream>
#include <Magick++.h>
#include <utility>

namespace fs = boost::filesystem;
namespace po = boost::program_options;
namespace im = Magick;

const std::string JPEG_EXTENSION(".jpg");
const std::string PDF_EXTENSION(".pdf");
const std::string PNG_EXTENSION(".png");

/// Brief description.
bool recurseAlbum(const fs::path& albumPath, const ConfigMap& config, const fs::path& sitePath, const fs::path& stylePath)
{
    bool status = EXIT_SUCCESS;
    std::cout << "recurseAlbum(" << albumPath << ", ...)" << std::endl;

    fs::path indexPath(albumPath.string() + IndexWriter::SEPARATOR + IndexWriter::INDEX_HTML);
    IndexWriter indexDoc(config, indexPath, sitePath, stylePath);

    for (fs::directory_iterator iter(albumPath); iter!=fs::directory_iterator(); ++iter)
    {
        fs::path currPath(iter->path());

        if (is_regular_file(currPath))
        {
            std::string filename(currPath.filename().string());
            std::string extension(currPath.extension().string());

            if ((filename.find(ThumbnailWriter::THUMB_PREFIX) != 0) &&
               ((extension == JPEG_EXTENSION) ||
                (extension == PNG_EXTENSION)))
            {
                im::Image image;
                image.ping(currPath.string());
                indexDoc.insertImage(currPath, image.columns(), image.rows());
            }

//~~        else if (extension == PDF_EXTENSION)
//~~        {
//~~            std::cout << "**recurseAlbum(...) : PDF=" << filename << std::endl;
//~~        }
        }
    }

    for (fs::directory_iterator iter(albumPath); iter!=fs::directory_iterator(); ++iter)
    {
        fs::path albumPath(iter->path());
        std::string albumName(albumPath.filename().string());

        if ((fs::is_directory(albumPath)) &&
           ((albumName != StyleWriter::CSS_DIR) &&
            (albumName != ThumbnailWriter::THUMBS_DIR)))
        {
            indexDoc.insertAlbumName(albumName);
            status |= recurseAlbum(albumPath, config, sitePath, stylePath);
        }
    }

    status |= indexDoc.write();
    return status;
}

int main(int argc, char** argv)
{
    bool status = EXIT_SUCCESS;

    try
    {
        ConfigMap config;
        std::string albumRoot;
        std::string analyticsId;
        bool chronological = false;
        std::string version(ALBUMIZER_APPNAME + std::string(" Version: ") + ALBUMIZER_VERSION_STRING);

        // Declare the supported CLI options.
        po::options_description description("Usage:\n[--help]\nAllowed options");
        description.add_options()
            ("help", "Produce help message")
            ("album-root", po::value<std::string>(&albumRoot)->required(), "Album root directory")
            ("analytics-id", po::value<std::string>(&analyticsId), "Google Analytics tracking ID")
            ("chronological", po::bool_switch(&chronological), "List albums in chronological order");

        po::variables_map varMap;
        po::store(po::parse_command_line(argc, argv, description), varMap);
        po::notify(varMap);

        if (varMap.count("help"))
        {
            std::cout << version << std::endl;
            std::cout << description << std::endl;
            return status;
        }

        config.setConfig(ConfigMap::ALBUM_ROOT, albumRoot);
        config.setConfig(ConfigMap::ANALYTICS_ID, analyticsId);
        config.setConfig(ConfigMap::CHRONOLOGICAL, chronological);
        fs::path rootPath(albumRoot);

        if (fs::is_directory(rootPath))
        {
            fs::path sitePath(IndexWriter::getSitePath(rootPath));
            fs::path stylePath(rootPath.string() + StyleWriter::SEPARATOR + StyleWriter::DEFAULT_PATH);
            StyleWriter styleDoc(stylePath);

            styleDoc.mkdir();
            status |= styleDoc.write();
            status |= recurseAlbum(rootPath, config, sitePath, stylePath);
        }

        else
        {
            std::cerr << description << std::endl;
            std::cerr << "--album-root argument: [" << albumRoot << "] does not name a valid directory" << std::endl;
            status = EXIT_FAILURE;
            std::cout << "main(...) => album-root: status=" << status << std::endl;
        }
    }

    catch(po::error& e)
    {
        std::cout << e.what() << std::endl;
        std::cout << "main(...) => po exception: status=" << status << std::endl;
        status = EXIT_FAILURE;
    }

    catch(im::Exception& e)
    {
        std::cout << e.what() << std::endl;
        std::cout << "main(...) => im exception: status=" << status << std::endl;
        status = EXIT_FAILURE;
    }

    return status;
}
