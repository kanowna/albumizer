#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

class ThumbnailWriter
{
public:
    ThumbnailWriter(const fs::path& imagePath);
    bool exists();
    fs::path getThumbPath();
    static size_t getThumbWidth(const size_t width, const size_t height);
    bool write();

    static const std::string SEPARATOR;
    static const std::string THUMBS_DIR;
    static const std::string THUMB_EXTENSION;
    static const std::string THUMB_PREFIX;
    static const size_t THUMB_HEIGHT;

private:
    const fs::path m_imagePath;
    fs::path m_thumbPath;
};
