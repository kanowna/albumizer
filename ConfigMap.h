#include <map>
#include <string>

class ConfigMap
{
public:
    typedef enum {
        ALBUM_ROOT = 0,
        ANALYTICS_ID,
        CHRONOLOGICAL,
        SITE_PATH
    } ConfigKeyType;

    ConfigMap();

    std::string getConfig(const ConfigKeyType& key) const;

    void setConfig(const ConfigKeyType& key, const bool value);
    void setConfig(const ConfigKeyType& key, const std::string value);

private:
    typedef std::pair<ConfigKeyType, std::string> ConfigPairType;
    typedef std::map<ConfigKeyType, std::string> ConfigMapType;

    ConfigMapType m_configMap;
};
