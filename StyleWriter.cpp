#include "StyleWriter.h"
#include <fstream>
#include <sstream>

const std::string StyleWriter::CSS_DIR("css");
const uint32_t StyleWriter::GOBBLE_SIZE = 0x400;
const std::string StyleWriter::SEPARATOR(1, fs::path::preferred_separator);
const std::string StyleWriter::STYLES_CSS("styles.css");
const std::string StyleWriter::DEFAULT_PATH(CSS_DIR + SEPARATOR + STYLES_CSS);

StyleWriter::StyleWriter(const fs::path& stylePath)
: m_stylePath(stylePath)
{
}

bool StyleWriter::mkdir()
{
    fs::path cssPath(m_stylePath.parent_path());
    bool created = fs::create_directory(cssPath);
    return (created?EXIT_SUCCESS:EXIT_FAILURE);
}

bool StyleWriter::write()
{
    bool status = EXIT_FAILURE;
    std::string inputBuffer;
    std::ifstream inputStream(m_stylePath.string(), std::ios_base::in | std::ios::binary);

    while (inputStream.good())
    {
        char gobble[GOBBLE_SIZE] = {0};
        inputStream.read(&gobble[0], GOBBLE_SIZE);
        inputBuffer.append(gobble, inputStream.gcount());
    }
    inputStream.close();

    std::ostringstream outputStream(m_stylePath.string(), std::ios_base::out | std::ios::trunc);
    if (outputStream.good())
    {
        outputStream << "body {" << std::endl;
        outputStream << "  color: black;" << std::endl;
        outputStream << "  font-family:verdana;" << std::endl;
        outputStream << "}" << std::endl;
        outputStream << std::endl;
        outputStream << ".flex-container {" << std::endl;
        outputStream << "  display: flex;" << std::endl;
        outputStream << "  flex-wrap: wrap;" << std::endl;
        outputStream << "  justify-content: center;" << std::endl;
        outputStream << "  list-style: none;" << std::endl;
        outputStream << "  margin: 0px;" << std::endl;
        outputStream << "}" << std::endl;
        outputStream << std::endl;
        outputStream << "h1 {" << std::endl;
        outputStream << "  display: block;" << std::endl;
        outputStream << "  font-size: 2em;" << std::endl;
        outputStream << "  text-transform: none;" << std::endl;
        outputStream << "  letter-spacing: 0;" << std::endl;
        outputStream << "  font-weight: 400;" << std::endl;
        outputStream << "}" << std::endl;
        outputStream << std::endl;
        outputStream << "h2 {" << std::endl;
        outputStream << "  display: block;" << std::endl;
        outputStream << "  font-size: 1.5em;" << std::endl;
        outputStream << "  text-transform: none;" << std::endl;
        outputStream << "  letter-spacing: 0;" << std::endl;
        outputStream << "  font-weight: 300;" << std::endl;
        outputStream << "}" << std::endl;
        outputStream << std::endl;
        outputStream << "img.slide {" << std::endl;
        outputStream << "  border-radius: 8px;" << std::endl;
        outputStream << "  display: none;" << std::endl;
        outputStream << "  padding-top: 2px;" << std::endl;
        outputStream << "  padding-left: 2px;" << std::endl;
        outputStream << "  padding-right: 2px;" << std::endl;
        outputStream << "  padding-bottom: 2px;" << std::endl;
        outputStream << "}" << std::endl;
        outputStream << std::endl;
        outputStream << "img.thumb {" << std::endl;
        outputStream << "  border-radius: 8px;" << std::endl;
        outputStream << "  display: block;" << std::endl;
        outputStream << "  padding-top: 2px;" << std::endl;
        outputStream << "  padding-left: 2px;" << std::endl;
        outputStream << "  padding-right: 2px;" << std::endl;
        outputStream << "  padding-bottom: 2px;" << std::endl;
        outputStream << "}" << std::endl;
        outputStream << std::endl;
        outputStream << ".pudding {" << std::endl;
        outputStream << "  padding-left: 12%;" << std::endl;
        outputStream << "  padding-right: 12%;" << std::endl;
        outputStream << "}" << std::endl;
        outputStream << std::endl;
        outputStream << "ul {" << std::endl;
        outputStream << "  display: block;" << std::endl;
        outputStream << "  list-style-type: disc;" << std::endl;
        outputStream << "  margin-block-start: 1em;" << std::endl;
        outputStream << "  margin-block-end: 1em;" << std::endl;
        outputStream << "  margin-inline-start: 0px;" << std::endl;
        outputStream << "  margin-inline-end: 0px;" << std::endl;
        outputStream << "  padding-inline-start: 40px;" << std::endl;
        outputStream << std::endl;
        outputStream << "  font-size: 1.2em;" << std::endl;
        outputStream << "  text-transform: none;" << std::endl;
        outputStream << "  letter-spacing: 0;" << std::endl;
        outputStream << "  font-weight: 250;" << std::endl;
        outputStream << "}" << std::endl;
        status = EXIT_SUCCESS;
    }

    std::string outputBuffer(outputStream.str());
    if (inputBuffer != outputBuffer)
    {
        std::ofstream styleStream(m_stylePath.string(), std::ios_base::out | std::ios::trunc);
        styleStream << outputBuffer;
        styleStream.close();
    }

    return status;
}
