#include "ThumbnailWriter.h"
#include <fstream>
#include <Magick++.h>
#include <sstream>

namespace im = Magick;

const std::string ThumbnailWriter::SEPARATOR(1, fs::path::preferred_separator);
const std::string ThumbnailWriter::THUMBS_DIR("thumbs");
const std::string ThumbnailWriter::THUMB_EXTENSION(".png");
const std::string ThumbnailWriter::THUMB_PREFIX("thumb_");
const size_t ThumbnailWriter::THUMB_HEIGHT = 256;

ThumbnailWriter::ThumbnailWriter(const fs::path& imagePath)
: m_imagePath(imagePath)
{
    std::string imageStem(imagePath.stem().string());
    std::string imageName(imagePath.filename().string());
    std::string thumbPath(imagePath.parent_path().string() + SEPARATOR + THUMBS_DIR + SEPARATOR + imageStem + THUMB_EXTENSION);
    m_thumbPath = fs::path(thumbPath);
}

bool ThumbnailWriter::exists()
{
    bool thumbExists = fs::exists(m_thumbPath);
    return thumbExists;
}

fs::path ThumbnailWriter::getThumbPath()
{
    return m_thumbPath;
}

size_t ThumbnailWriter::getThumbWidth(const size_t width, const size_t height)
{
    size_t thumbWidth = ThumbnailWriter::THUMB_HEIGHT;

    if (height)
    {
        thumbWidth = width * ThumbnailWriter::THUMB_HEIGHT / height;
    }

    return thumbWidth;
}

bool ThumbnailWriter::write()
{
    bool status = EXIT_SUCCESS;
    fs::create_directory(m_thumbPath.parent_path());
    im::Image thumb(m_imagePath.string());
    size_t thumbWidth = getThumbWidth(thumb.columns(), thumb.rows());
    thumb.resize(im::Geometry(thumbWidth, THUMB_HEIGHT));
    thumb.write(m_thumbPath.string());
    return status;
}
