#include <boost/filesystem.hpp>

namespace fs = boost::filesystem;

class StyleWriter
{
public:
    StyleWriter(const fs::path& stylePath);
    bool mkdir();
    bool write();

    static const std::string CSS_DIR;
    static const uint32_t GOBBLE_SIZE;
    static const std::string SEPARATOR;
    static const std::string STYLES_CSS;
    static const std::string DEFAULT_PATH;

private:
    const fs::path m_stylePath;
};
